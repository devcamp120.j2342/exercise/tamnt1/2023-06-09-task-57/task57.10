package com.example.saleapi.services;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.time.LocalDate;
import java.time.format.TextStyle;

import org.springframework.stereotype.Service;

@Service
public class SaleService {
    public List<String> createSaleForWeek() {
        List<String> salePromotions = new ArrayList<>();

        // Add sale promotions for each day of the week
        salePromotions.add("Mua một tặng 1"); // Monday
        salePromotions.add("Tặng nước uống"); // Tuesday
        salePromotions.add("Giảm 50%"); // Wednesday
        salePromotions.add("Mua một tặng 1"); // Thursday
        salePromotions.add("Tặng Cake"); // Friday
        salePromotions.add("Tặng nước uống"); // Saturday
        salePromotions.add("Mua một tặng 1"); // Sunday

        return salePromotions;
    }

    public String getCurrentDayOfWeek() {
        DayOfWeek currentDayOfWeek = LocalDate.now().getDayOfWeek();
        Locale locale = new Locale("vi", "VN");
        return currentDayOfWeek.getDisplayName(TextStyle.FULL, locale);
    }

    public String getSaleForCurrentDay() {
        List<String> salePromotions = createSaleForWeek();
        String currentDayOfWeek = getCurrentDayOfWeek();
        int dayOfWeekValue = LocalDate.now().getDayOfWeek().getValue() - 1;
        String salePromotion = salePromotions.get(dayOfWeekValue);
        System.out.println(currentDayOfWeek + ", " + salePromotion);
        return currentDayOfWeek + ", " + salePromotion;
    }

}
