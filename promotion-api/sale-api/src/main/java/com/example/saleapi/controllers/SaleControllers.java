package com.example.saleapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.saleapi.services.SaleService;

@RestController
@CrossOrigin
public class SaleControllers {
    @Autowired
    private SaleService saleService;

    @GetMapping("/on-sale")
    public String getSaleInfo() {
        return saleService.getSaleForCurrentDay();
    }
}